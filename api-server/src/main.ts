import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { SWAGGER_DESCRIPTION, DEFAULT_HTTP_PORT,  CONFIGURATION } from './app.constants';
import {ValidationPipe} from "@nestjs/common";
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);


  const configService  = app.get(ConfigService)
  
  app.setGlobalPrefix(configService.get<string>(CONFIGURATION.API_PREFIX));
  app.useGlobalPipes(new ValidationPipe());

  const swaggerOptions = new DocumentBuilder()
    .setTitle(SWAGGER_DESCRIPTION.TITLE)
    .setDescription(SWAGGER_DESCRIPTION.DESCRIPTION)
    .setVersion(SWAGGER_DESCRIPTION.VERSION)
    .addBearerAuth()
    .build();
  const swaggerDocument = SwaggerModule.createDocument(app, swaggerOptions);

  SwaggerModule.setup(configService.get<string>(CONFIGURATION.SWAGGER_PREFIX), app, swaggerDocument);

  app.enableCors();


  

  await app.listen( parseInt(configService.get<string>(CONFIGURATION.HTTP_PORT))  || DEFAULT_HTTP_PORT);
}
bootstrap();
