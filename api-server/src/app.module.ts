import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { CONFIGURATION,DEFAULT_ENV_FILE } from './app.constants';
import { config } from './config/env.config';
import { NewModule } from './modules/news/new.module';

@Module({
  imports: [ConfigModule.forRoot(
    {
      isGlobal: true,
      envFilePath : DEFAULT_ENV_FILE,
      load : [config]
    },
  ),
    MongooseModule.forRootAsync({
    imports : [ConfigModule],
    useFactory : async (configService : ConfigService) => ({
      uri : configService.get<string>(CONFIGURATION.MONGO_URI),
      useNewUrlParser : true,
      useUnifiedTopology : true
    }),
    inject : [ConfigService]
  }),
    ScheduleModule.forRoot(),
    NewModule
  ]
})
export class AppModule {}
