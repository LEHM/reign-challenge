export const NEWS_MODEL = "NEWS_MODEL"
export const NEWS_COLLECTION = "news"
export const NEWS_CONTROLLER = {

    CONTROLLER_ROUTE : 'news',
    FIND_ALL : 'list',
    DELETE_ONE : 'delete/:idNew',
    CREATE_ONE : 'create'
}

export const TIMEZONE_AMERICA_LIMA = "America/Lima"

export const DATE_FNS_FORMAT = {
    FORMAT : 'YYYY-MM-DD HH:mm:ss ',
    SAMEDAY: 'hh:mm a',
    NEXTDAY: '[Tomorrow]',
    NEXTWEEK: 'dddd',
    LASTDAY: '[Yesterday]',
    LASTWEEK: '[Last] dddd',
    SAMELSE : 'MMM DD',
    DEFAULTELSE : '[Other] dddd'
}