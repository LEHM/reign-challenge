import { HttpModule, Module, OnModuleInit } from '@nestjs/common';
import { NewsService } from './news.service';
import { NewsController } from './news.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { NEWS_MODEL } from './news.contants';
import { NewsSchema } from "./models/news.model";
import { ConfigModule } from '@nestjs/config';

@Module({
  imports:[MongooseModule.forFeature([{name : NEWS_MODEL, schema :NewsSchema }]),
          HttpModule,
          ConfigModule],
  controllers: [NewsController],
  providers: [NewsService],
  exports : [NewsService]
})
export class NewModule{}
