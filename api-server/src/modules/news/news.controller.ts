import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { NewsService } from './news.service';
import { NEWS_CONTROLLER } from './news.contants';
import { ApiNoContentResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreateItemDTO, ListItems } from './dto/news.dto';


@ApiTags(NEWS_CONTROLLER.CONTROLLER_ROUTE)
@Controller(NEWS_CONTROLLER.CONTROLLER_ROUTE)
export class NewsController {
  constructor(private readonly newService: NewsService) {}

  
  @ApiResponse({type : ListItems})
  @Get(NEWS_CONTROLLER.FIND_ALL)
  findAll() {
    return this.newService.findAll();
  }

  @ApiNoContentResponse({description : "Delete item"})
  @Delete(NEWS_CONTROLLER.DELETE_ONE)
  deleteOne(@Param('idNew')  idNew : string){
    return this.newService.deleteOne(idNew)
  }

  @Delete(NEWS_CONTROLLER.CREATE_ONE)
  create(@Body()  createItemDTO : CreateItemDTO){
    return this.newService.createOne(createItemDTO)
  }

}
