import { HttpService, Injectable, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {  InjectModel } from '@nestjs/mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { v4 as uuidv4 } from 'uuid';
import { Model } from 'mongoose';
import { from, Observable } from 'rxjs';
import { concatMap, filter, map, pluck, switchMap, tap, toArray } from 'rxjs/operators';
import { CONFIGURATION } from '../../app.constants';
import { CreateItemDTO, CreateNewsDto, ItemDTO, ListItems } from './dto/news.dto';
import { NewsDocument } from './models/news.model';
import { NEWS_MODEL } from './news.contants';
import { parseDate } from './utils/news.util';

@Injectable()
export class NewsService implements OnModuleInit{



  private HN_URI  : string;

  constructor(@InjectModel(NEWS_MODEL) private readonly newsModel : Model<NewsDocument>,
                                       private readonly httpService : HttpService,
                                       private readonly configService : ConfigService){
    this.HN_URI = this.configService.get<string>(CONFIGURATION.HN_URI)
    
  }

  onModuleInit() {
    this.fillDatabase().subscribe((_) => {
      console.log("Initializing database...");
      
    } )
  }

  @Cron(CronExpression.EVERY_HOUR)
  executeCron(){
    this.fillDatabase().subscribe((_) => {
      console.log("Sejereh nos -- filling database");
      
    } )
  }


  fillDatabase() {

    return  from(this.httpService.get(this.HN_URI)).pipe(
      map( axios => axios.data),
      map( data => data.hits),
      switchMap((hits : CreateNewsDto[]) => {
        return from(hits).pipe(
          concatMap(( newDTO : CreateNewsDto)=> {   
            return from(this.newsModel.findOne({_id : newDTO.objectID})).pipe(
              filter(result => result == null || result == undefined),
              concatMap((_)=> {

                const newsDocument : NewsDocument  = new this.newsModel()

                newsDocument._id  = newDTO.objectID
                newsDocument.title = newDTO.title
                newsDocument.author = newDTO.author
                newsDocument.storyTitle = newDTO.story_title
                newsDocument.storyUrl = newDTO.story_url
                newsDocument.tags = newDTO._tags
                newsDocument.url = newDTO.url
                newsDocument.storyId = newDTO.story_id
                newsDocument.points = newDTO.points
                newsDocument.storyId = newDTO.story_id
                newsDocument.numComments = newDTO.num_comments
                newsDocument.commentText = newDTO.comment_text
                newsDocument.createdAt = newDTO.created_at

                return from( newsDocument.save())

              })
            )
          })
        )
      }),
      toArray()
    )
  }

  findAll() : Observable<ListItems<ItemDTO>> {

    const query : object   = {
      isDeleted : false
    }

    return  from( this.newsModel.find(query).sort({
      createdAt : -1
    }).exec()).pipe(
      concatMap(resultSet => from(resultSet).pipe(
        filter(item => (item.title !== null || item.storyTitle !==null) ),
        filter(item => (item.storyUrl !==null || item.url !== null)), 
        map(newsItem => new ItemDTO(
          {
            _id : newsItem._id,
            author : newsItem.author,
            createdAt : parseDate(newsItem.createdAt),
            title : newsItem.title,
            storyTitle : newsItem.storyTitle,
            storyUrl : newsItem.storyUrl,
            url : newsItem.url
          }))
      )),toArray(),
      map(( items: ItemDTO[]) => {
        return new ListItems<ItemDTO>({
          items ,
          size : items.length
        })
      })
    )
  }


  createOne(  createItemDTO  : CreateItemDTO) : Observable<NewsDocument> {
  
    const  newsDocument :  NewsDocument = new this.newsModel();
    
    newsDocument._id = uuidv4()
    newsDocument.author = createItemDTO.author
    newsDocument.storyTitle  = createItemDTO.storyTitle
    newsDocument.url = createItemDTO.url
    newsDocument.title = createItemDTO.title
    newsDocument.storyUrl = createItemDTO.storyUrl
  
  
    return from(newsDocument.save()).pipe(
      pluck('_id')
    ) 
  }


  deleteOne(idNew : string) : Observable<String>{
    return from(  this.newsModel.findOneAndDelete({
      _id : idNew
    })).pipe(
      pluck('_id'),
      map(_id =>  _id )
    )
  }
}