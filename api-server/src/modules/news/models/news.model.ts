import { Document, Schema } from "mongoose"
import { NEWS_COLLECTION } from "../news.contants"


export interface INews{
    title : string
    url : string
    storyUrl : string
    storyTitle : string
    storyId : number
    createdAt : string
    author : string
    points : number
    storyText : string
    commentText : string
    tags : [string]
    numComments : number
    parentId : number
    createdAtI : number
    isDeleted : boolean
}

export interface  NewsDocument extends Document, INews{
    
}


export const NewsSchema = new Schema({


    _id : { type : String},
    title : String,
    url : String,
    storyUrl : String,
    storyTitle : String,
    storyId : Number,
    createdAt :String,
    points : Number,
    author : String,
    storyText : String,
    commentText : String,
    tags : [String],
    numComments : Number,
    parentId: Number,
    createdAtI : Number,
    isDeleted : {type : Boolean, default : false},
},{
    collection : NEWS_COLLECTION
})

