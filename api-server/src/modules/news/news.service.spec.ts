import { Test, TestingModule } from '@nestjs/testing';
import { NewsService } from './news.service';
import { v4 as uuidv4 } from 'uuid';
import { ListItems } from './dto/news.dto';

import { getModelToken } from '@nestjs/mongoose';
import {  ConfigService } from '@nestjs/config';


import { HttpModule } from '@nestjs/common';
import { NEWS_MODEL } from './news.contants';

describe('News Service', () => {
  
  let newsService: NewsService;
  

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule(
      {imports : [
        HttpModule],
        providers: [{
          provide : ConfigService,
          useValue: {
            get : (key : string) => {
              return "https://hn.algolia.com/api/v1/search_by_date?query=nodejs"
            }
          }
        },
          NewsService,{
          provide: getModelToken(NEWS_MODEL),
          useValue: {
            findOneAndDelete: jest.fn( async ({_id}) => {
              return {
                _id 
              }
            }),
            find : jest.fn((query ? : any) => {
              
                            
              const simpleDocument : any = {
                _id : uuidv4(),
                title : uuidv4(),
                storyUrl : uuidv4(),
                storyTitle : uuidv4(),
                url : uuidv4(),
                author : uuidv4(),
                createdAt : "2021-03-07T22:45:15.000Z",
              }

              return {
                sort: () => {
                  return {
                    exec: async () => {
                      return [simpleDocument]
                    }
                  }
                }
              }
            }),
            new : jest.fn(async (doc ?: any)=> {
              return {
                _id : doc._id,
                save:() => {
                  return {
                    _id : doc._id
                  }
                }
              }
            }),
            findOne : jest.fn(async ({_id}) => {
              return {
                _id : uuidv4()
              }
            })
          },
        },] 
      } 
    ).compile();

    newsService = module.get<NewsService>(NewsService);
    
  });

  it('This test should  return a string value', (done) => {


    let mockId: string  = uuidv4();

    newsService.deleteOne(mockId).subscribe(
      (pluckId) =>{
        expect(pluckId).toEqual(mockId)
        done()
      }
    )


  });
  it('This test should  expects a string value as a result', (done) => {


    
    let mockString : string = uuidv4()
    newsService.deleteOne(mockString).subscribe(
      (pluckId) =>{
        expect(typeof pluckId === "string" ).toBe(true)
        done()
      }
    )

  });


  it("This test should give us a 'ListItems<T>' object", (done) => {


    newsService.findAll().subscribe(
      (listItems) =>{
        expect(listItems instanceof ListItems).toBe(true)
        done()
      }
    )

  });



  it("This test should fill the database ", (done) => {

    newsService.fillDatabase().subscribe(
      (filledItems)=>{
        expect(Array.isArray(filledItems)).toBe(true)
        done()
      }
    )

  });
});
