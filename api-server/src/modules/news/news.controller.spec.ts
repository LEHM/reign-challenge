import { Test } from '@nestjs/testing';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import { v4 as uuidv4 } from 'uuid';
import {  of } from 'rxjs';
import { ItemDTO, ListItems } from './dto/news.dto';
import { NEWS_MODEL } from "./news.contants";
import { map } from 'rxjs/operators';
import { HttpModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { getModelToken } from '@nestjs/mongoose';

describe('NewsController', () => {
  let newsController: NewsController;
  let newsService: NewsService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
        imports : [HttpModule,ConfigModule],
        controllers: [NewsController],
        providers: [NewsService,{
            provide: getModelToken(NEWS_MODEL),
            useValue : {} 
        }],
      }).compile();

    newsService = moduleRef.get<NewsService>(NewsService);
    newsController = moduleRef.get<NewsController>(NewsController);
  });

  it("should return an array of 'ItemsDTO' ", (done) => {


        const items  : ItemDTO[] = [new ItemDTO({
            _id : uuidv4()
        }),new ItemDTO({
            _id : uuidv4()
        }),new ItemDTO({
            _id : uuidv4()
        })]  
        const listItems : ListItems<ItemDTO>  = new ListItems<ItemDTO>({
            items , 
            page : 25,
            size : 10
        })
    const result = of(listItems)
    jest.spyOn(newsService, 'findAll').mockImplementation(() => result);

    newsController.findAll().pipe(
        map((listItems : ListItems<ItemDTO>)=>{
            return listItems.items
        })
    ).subscribe((dataItems) => {
        expect(dataItems).toBe(items)
        done()
    })
});
    it("should return a string value ", (done) => {


    const mockId : string  = uuidv4()
    const result = of(mockId)
    jest.spyOn(newsService, 'deleteOne').mockImplementation((_id : string) => result);

    newsController.deleteOne(mockId)
        .subscribe((_id) => {
            expect( typeof _id === "string").toBe(true)
            done()
        })
    });
    it("this test should return the same Id that as passed as argument in the controller method", (done) => {


        const mockId : string  = uuidv4()
        const result = of(mockId)
        jest.spyOn(newsService, 'deleteOne').mockImplementation((_id : string) => result);
    
        newsController.deleteOne(mockId)
            .subscribe((_id) => {
                expect(_id).toEqual(mockId)
                done()
            })
        });
});