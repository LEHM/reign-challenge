import * as moment from "moment";
import { tz } from "moment-timezone";
import { DATE_FNS_FORMAT, TIMEZONE_AMERICA_LIMA } from "../news.contants";

export const parseDate  = ( date : string) => {

    if((tz(TIMEZONE_AMERICA_LIMA).dayOfYear() - moment.utc(date,DATE_FNS_FORMAT.FORMAT).dayOfYear()) <= 0){
        return moment.utc(date,DATE_FNS_FORMAT.FORMAT).format(DATE_FNS_FORMAT.SAMEDAY)
    }else{
        if ((tz(TIMEZONE_AMERICA_LIMA).dayOfYear() - moment.utc(date,DATE_FNS_FORMAT.FORMAT).dayOfYear()) ==  1) {
            return moment.utc(date,DATE_FNS_FORMAT.FORMAT).format(DATE_FNS_FORMAT.LASTDAY)
        }else{
            return moment.utc(date,DATE_FNS_FORMAT.FORMAT).format(DATE_FNS_FORMAT.SAMELSE)
        }
    }
    



}


 