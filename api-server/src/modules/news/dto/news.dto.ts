export class CreateNewsDto{

   
    title : string

    
    url : string
    
    
    story_url : string
    
  
    story_title : string
    
  
    story_id : number
    
  
    created_at : string
    
  
    author : string
    
   
    points : number
    
   
    story_text : string
    
   
    comment_text : string
    
   
    _tags : [string]
    
  
    num_comments : number
    
  
    parent_id : number
    
   
    created_at_i : number
    
  
    objectID    :   string


    isDeleted : boolean


    constructor( newsDTO : Partial<CreateNewsDto>){
        Object.assign(this,newsDTO)
    }

}


export class ListItems<T>{


    page : number
    size : number
    items : Array<T>


    constructor( items : Partial<ListItems<T>>){
        Object.assign(this,items)
    }

}

export class ItemDTO{

    _id : string
    title : string
    storyTitle : string
    storyUrl : string
    author : string 
    url : string
    createdAt  : string


    constructor( item : Partial<ItemDTO>){
        Object.assign(this,item)
    }

}

export class CreateItemDTO{

    storyTitle : string
    storyUrl : string
    author : string
    title : string
    url : string

    constructor(item : Partial<CreateItemDTO>){
        Object.assign(this,item)
    }
}