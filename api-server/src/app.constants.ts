export const DEFAULT_ENVIRONMENT = `${process.env.NODE_ENV || 'development'}.env`
export const DEFAULT_ENV_FILE = `${process.cwd()}/environments/${DEFAULT_ENVIRONMENT}`


export const CONFIGURATION = {
    MONGO_URI : 'MONGO_URI',
    HN_URI : 'HN_URI',
    HTTP_PORT :'HTTP_PORT',
    API_PREFIX : 'API_PREFIX',
    SWAGGER_PREFIX : 'SWAGGER_PREFIX'

}


export const DEFAULT_HTTP_PORT = 5000
export const SWAGGER_DESCRIPTION = {
    TITLE : 'REIGN - Services',
    DESCRIPTION : 'REST Services',
    VERSION :'1.0'
}
