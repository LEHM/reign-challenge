export const  config  = () =>({
    MONGO_URI:process.env.MONGO_URI,
    HN_URI:process.env.HN_URI,
    HTTP_PORT : process.env.HTTP_PORT,
    SWAGGER_PREFIX : process.env.SWAGGER_PREFIX,
    API_PREFIX : process.env.API_PREFIX
})
