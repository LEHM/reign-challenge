import { useEffect, useState } from 'react';
import axios from "axios";
import { ROUTE } from '../constants/app.constants';
import { pipelineRoute } from '../utils/string.util';

export const useApiHook = () => {



    const [list,setList] = useState([])

    const deleteItem = (_id) => { 
        
        axios.delete(pipelineRoute(process.env.REACT_APP_SERVER_API,ROUTE.DELETE,_id)).then(
        res => res.data
      ).then(
        (_) => {
          
          let newList  = list.filter(value => value._id !== _id)
          
          setList(newList)
        }
      )
    }

    useEffect(() => {
    
        async function listNews() {

          const result = await axios.get(pipelineRoute(process.env.REACT_APP_SERVER_API,ROUTE.LIST))
    
          setList(result.data.items);
    
        }
        listNews()
    
      },[])


      return {
          list,deleteItem
      }
}
