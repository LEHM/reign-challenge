import trashImage from "./../assets/images/trash.png";
import './Row.css';

import { useMedia  } from "react-use";
import React, { useState } from 'react'




function Row({ row , ...rest}) {

    const [visible,setVisible] = useState(false)

    function onClick(e,row) {

        window.open(row.storyUrl || row.url, "_blank");
    }

    const isWidth = useMedia('(max-width:850px)')

    return (<div onClick={e=> onClick(e,row)}  onMouseOver={() => setVisible(true)}  onMouseLeave={() => setVisible(false)} className="item">
    <div className="left">
      <span style={isWidth ? {color : "#333"} :  {marginRight:15, color: "#333"}}>{row.title || row.storyTitle}</span>
      <span style={isWidth ? {color : "#999", marginTop:10} :  {color : "#999"}}> { isWidth ?  row.author  :  " - " + row.author }</span>
    </div>
    <div className="right">
      <span style={isWidth ? {} :  {marginRight:25}}>{row.createdAt}</span> 
      <img  onClick={e => rest.deleteRow(e,row)}  style={   isWidth ? {marginTop : 5, visibility : visible ? "visible"  : "hidden"} :   {visibility : visible ? "visible" : "hidden"}}  width={ isWidth ? 24 : 32} height={ isWidth ? 24:  32} src={  trashImage  } alt={"delete"}/>
    </div>
  </div>)
}


export default Row

