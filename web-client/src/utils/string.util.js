
export const pipelineRoute =    (...args) => {
    return args.reduce((previousValue,currentValue) => previousValue + "/" + currentValue) 
}