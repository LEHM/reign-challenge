
import './App.css';

import Row from './components/Row';
import { useApiHook } from './hooks/use-api.hook';

function App() {
  
  const {list,deleteItem} = useApiHook()


  function deleteRow(e, row) {
    e.stopPropagation()
    deleteItem(row._id)    
  }
  

  return (
    <div className="container">
      <div className="header">
        <span style={{color:"white",fontSize:60,marginTop:40}}>HN Feed</span>
        <span style={{color:"white",fontSize:20,marginTop:15}}>We love Hacker News!</span>
      </div>
      <div className="list">
        {list.map((row , index)=> {
            return <Row  key={index} row={row}  deleteRow={deleteRow}  />
        })}
      </div>
    </div>
  );
}

export default App;
