# Reign challenge
## Requirements
- docker
- docker-compose
- mongo-server
- mongo-client
## Configuration
- If you want to change the URI of MongoDB then you can modify the .env files found inside the environments folder located at ./server-api/environments.
## Gather up
- First we need to change the host's data storage path to start the service. The default path should be changed to any path located on the host to manage data storage. Example:
/home/luis/mongo/db --> {$HOME}/mongo/db where {$HOME} is the current location by the logged user. That value should be change to gather up the project.
- Then, with docker the only command that we need is --> docker-compose up --build. Then check in the browser the next route --> http://localhost:4000/.
## Refresh data
- The data will be loaded to the app on the moment docker-compose finish the build process. 
## Run server tests
To get the test coverage you just need to run these commands. 
- $ cd api-server/ 
- $ npm install
- $ npm run test:cov
## Swagger documentation
- To check the app documentation you can go to the next location --> http://localhost:5000/documentation/ 